﻿using CU.OnBoarding.Helper;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace CU.OnBoarding.DataAccess
{
    public class Function : ConnectionHelper
    {
        public void SendList()
        {
            //Call logic for getting list for Today by Weekday or Daily
            ConfigurationService cs = new ConfigurationService();
            var lst = cs.ListConfigurationsWeekDayName(DateTime.UtcNow.ToString("dddd").ToUpper());
            //logic to get Monthly Date list as on Today and add it to Existing list 
            var monthlst = cs.ListConfigurationsbyMonthDay(DateTime.UtcNow.Day.ToString());
            foreach (var objconfig in monthlst)
            {
                lst.Add(objconfig);
            }
            //loop through list of configuration and Call List procedure to get date and send CSV file in Email as attachment
            foreach (var objconfig in lst)
            {
                DataHelper ds = new DataHelper();
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@U_id", objconfig.UserId);
                param[1] = new SqlParameter("@List_type", objconfig.ListType);
                param[2] = new SqlParameter("@Frequency", objconfig.ListFrequency);
                var CSVResult = ds.ExecuteStoredProcedureDataTable("Member_list", param);
                byte[] result = null;
                if (CSVResult != null)
                {
                    result = FileExportHelper.DataTableToCSV(CSVResult);
                }
                SendEmail(objconfig.UserEmail, objconfig.ListType + " List on " + DateTime.UtcNow.ToString("MM/dd/yyyy"), "", result, objconfig.ListType);
                //Log sent email details 
                EmailTemplateService es = new EmailTemplateService();
                es.EmailLog_InsertOrUpdate(new Model.EmailLog
                {
                    AttchmentFileName = objconfig.ListType + ".csv",
                    EmailSentDt = DateTime.Now,
                    FileData = result,
                    Subject = objconfig.ListType + " List on " + DateTime.UtcNow.ToString("MM/dd/yyyy"),
                    ToEmail = objconfig.UserEmail
                });

            }
        }

        public void SendEmail(string receiver, string subject, string EmailMessage, byte[] bytearray, string ListType)
        {
            try
            {
                var body = "<p>Email From: {0} ({1})</p><p>Message: {2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(receiver));  // replace with valid value 
                message.From = new MailAddress("info@cucompare.com");  // replace with valid value
                message.Subject = subject;
                message.Body = string.Format(body, "CU OnBoarding", "info@cucompare.com", "Please find attached List for your reference");
                message.IsBodyHtml = true;
                if (bytearray != null)
                {
                    MemoryStream stream = new MemoryStream(bytearray);
                    //Add a new attachment to the E-mail message, using the correct MIME type
                    Attachment attachment = new Attachment(stream, new ContentType("text/csv"));
                    attachment.Name = ListType + ".csv";
                    message.Attachments.Add(attachment);

                }

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "info@cucompare.com",  // replace with valid value
                        Password = "Cmdeie#220"  // replace with valid value
                    };
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.office365.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
