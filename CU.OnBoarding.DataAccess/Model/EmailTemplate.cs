﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CU.OnBoarding.DataAccess.Model
{
    public class EmailTemplate
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string SenderName { get; set; }

        public string SenderEmail { get; set; }

        public string EmailSubject { get; set; }

        public string EmailBody { get; set; }

        public DateTime CreatedDate { get; set; }
    }
    public class EmailLog
    {

        public int Id { get; set; }

        public string ToEmail { get; set; }

        public string Subject { get; set; }

        public string AttchmentFileName { get; set; }

        public byte[] FileData { get; set; }

        public DateTime EmailSentDt { get; set; }
    }

    public class EmailTemplateViewModel
    { 
      public List<EmailTemplate> lstResults { get; set; }
    }
    public class EmailLogViewModel
    {
        public List<EmailLog> lstResults { get; set; }
    }
}
