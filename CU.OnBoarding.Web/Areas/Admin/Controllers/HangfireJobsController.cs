﻿using CU.OnBoarding.DataAccess;
using CU.OnBoarding.Web.Areas.Admin.Models;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CU.OnBoarding.Web.Areas.Admin.Controllers
{
    public class HangfireJobsController : BaseController
    {
        // GET: Admin/HangfireJobs
        public ActionResult Index()
        {
            return View(new Models.HangFireJobsViewModel());
        }

        [HttpPost]
        public ActionResult Index(List<int> selected)
        {
            if (selected != null && selected.Any())
            {
                foreach (var item in selected)
                {
                    // minutes, hours, days, months, days of week
                    switch ((HangFireJobsViewModel.JobEnum)item)
                    {
                        case HangFireJobsViewModel.JobEnum.SendEmail:
                            RecurringJob.AddOrUpdate<Function>("Send Email", ss => ss.SendList(), "0 2 * * *", TimeZoneInfo.Local); // run daily 2 AM UTC 
                            break;
                        case HangFireJobsViewModel.JobEnum.UpdateCountofDays:
                            RecurringJob.AddOrUpdate<ConfigurationService>("Update Count of Days Daily", fs => fs.UpdateDayConfiguration(), "0 1 * * *", TimeZoneInfo.Local); // run Daily at 1 AM UTC
                            break;
                    }
                }

                return RedirectToAction("recurring", "hangfire", new { area = "" });
            }


            return Content("Completed");
        }
    }
}