﻿using CU.OnBoarding.DataAccess;
using CU.OnBoarding.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CU.OnBoarding.Web.Controllers
{
    public class EmailLogController : BaseController
    {
        // GET: EmailLog
        public ActionResult Index()
        {
            EmailLogViewModel model = new EmailLogViewModel();
            EmailTemplateService es = new EmailTemplateService();
            model.lstResults = es.EmailLogs.ToList();
            return View(model);
        }

        public ActionResult DownLoadAttchment(int Id)
        {
            EmailTemplateService es = new EmailTemplateService();
            var data = es.GetEmailFileContent(Id);
            if (data != null)
            {
                return File(data.FileData.ToArray(), "application/text", data.AttchmentFileName);
            }
            return Content("No data found for file");
        }
    }
}